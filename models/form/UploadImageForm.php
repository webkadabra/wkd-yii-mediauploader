<?php
/**
 * @author Sergii Gamaiunov <hello@webkadabra.com>
 */
class UploadImageForm extends CFormModel
{
    public $uploadedFile;
 
    public function rules()
    {
        return array(
            array(
                'uploadedFile',
                'safe',
            ),
        );
    }
}