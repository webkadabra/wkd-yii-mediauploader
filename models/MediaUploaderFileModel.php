<?php

/**
 * This is the model class for table "media_uploader_file_model".
 *
 * The followings are the available columns in table 'media_uploader_file_model':
 * @property string $id
 * @property string $file_id
 * @property string $model_pk
 * @property string $model_class_name
 * 
 * @author Sergii Gamaiunov <hello@webkadabra.com>
 */
class MediaUploaderFileModel extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MediaUploaderFileModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'media_uploader_file_model';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('file_id, model_pk, model_class_name', 'required'),
			array('file_id', 'length', 'max'=>11),
			array('model_pk', 'length', 'max'=>10),
			array('model_class_name', 'length', 'max'=>30),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, file_id, model_pk, model_class_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'file_id' => 'File',
			'model_pk' => 'Model Pk',
			'model_class_name' => 'Model Class Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('file_id',$this->file_id,true);
		$criteria->compare('model_pk',$this->model_pk,true);
		$criteria->compare('model_class_name',$this->model_class_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}