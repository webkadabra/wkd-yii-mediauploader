<?php

/**
 * This is the model class for table "media_uploader_file".
 *
 * The followings are the available columns in table 'media_uploader_file':
 * @property string $id
 * @property string $folder
 * @property string $file_name
 * @property string $original_filename
 * @property string $modified_on
 * @property string $owner_user_id
 * @property string $name
 * @property string $caption
 * @property string $ext
 *
 * @author Sergii Gamaiunov <hello@webkadabra.com>
 */
class MediaUploaderFile extends CActiveRecord
{
	public static $extMap = array(
		'image' => array('jpg', 'gif', 'png'),
		'audio' => array('mp3', 'flac', 'wav'),
		'video' => array('mp4', 'mpg', 'avi', 'wmv', 'mpeg'),
		'file' => array('zip', 'rar', 'gzip', 'bzip', '7z', 'psd', 'doc', 'docx', 'pdf', 'djvu', 'txt', 'xls', 'odt'),
	);

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MediaUploaderFile the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'media_uploader_file';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('file_name', 'required'),
			array('folder, file_name, original_filename, name', 'length', 'max' => 100),
			array('owner_user_id', 'length', 'max' => 10),
			array('ext', 'length', 'max' => 4),
			array('modified_on, caption', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, folder, file_name, original_filename, modified_on, owner_user_id, name, caption, ext', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'RelatedModelsIDs' => array(self::HAS_MANY, 'MediaUploaderFileModel', 'file_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'folder' => 'Folder',
			'file_name' => 'File Name',
			'original_filename' => 'Original Filename',
			'modified_on' => 'Modified On',
			'owner_user_id' => 'Owner User',
			'name' => 'Name',
			'caption' => 'Caption',
			'ext' => 'Ext',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public $filterByModel;

	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('folder', $this->folder, true);
		$criteria->compare('file_name', $this->file_name, true);
		$criteria->compare('original_filename', $this->original_filename, true);
		$criteria->compare('modified_on', $this->modified_on, true);
		$criteria->compare('owner_user_id', $this->owner_user_id, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('caption', $this->caption, true);
		$criteria->compare('ext', $this->ext, true);

		if ($this->filterByModel) {
			foreach ($this->filterByModel as $modelName => $modelID) {
				$criteria->addCondition('RelatedModelsIDs.model_pk=:modelPK');
				$criteria->addCondition('RelatedModelsIDs.model_class_name=:modelClassName');
				$criteria->params[':modelPK'] = $modelID;
				$criteria->params[':modelClassName'] = $modelName;
			}

			$criteria->together = true;
			$criteria->with[] = 'RelatedModelsIDs';
		}

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function getName()
	{
		if ($this->name)
			return $this->name;

		if (!$this->originalFilename)
			return false;

		$ext = strrpos($this->originalFilename, '.');
		$fileName = substr($this->originalFilename, 0, $ext);

		return $fileName;
	}

	public function getLink()
	{
		return trim(Yii::app()->params['frontend.url'], '/') . '/' . trim($this->folder, '/') . '/' . $this->file_name;
	}

	/**
	 * @return string
	 */
	public function getInsertCode()
	{
		if ($this->isImage()) {
			#$insertHtml = '<a href="' . $this->getLink() . '">' . CHtml::image($this->getLink(), $this->name, array('class' => 'att-image-' . $this->id . ' alignleft size-full')) . '</a>';
			$insertHtml = '<a href="' . $this->getLink() . '">' . CHtml::image($this->getLink(), $this->name, array('class' => 'att-image-' . $this->id . ' size-full')) . '</a>';
		} else {
			$insertHtml = '<a href="' . $this->getLink() . '">' . ($this->name ? CHtml::encode($this->name) : $this->getLink()) . '</a>';
		}

		return $insertHtml;
	}

	public function isImage()
	{
		return in_array($this->ext, self::$extMap['image']);
	}

	/**
	 * ��������� ����� ����� ������� ������ � ����� ������� CActiveRecord
	 */
	public function boundByModel($model)
	{
		$modelPK = $model->id;
		$modelClassName = get_class($model);
		// Already has connection?
		$exists = MediaUploaderFileModel::model()->find('model_pk=? AND model_class_name=? AND file_id=?', array(
			$modelPK,
			$modelClassName,
			$this->id,
		));

		if (!$exists) {
			$newRecord = new MediaUploaderFileModel();
			$newRecord->model_pk = $modelPK;
			$newRecord->model_class_name = $modelClassName;
			$newRecord->file_id = $this->id;
			return $newRecord->save(false);
		}

		return true;
	}
}