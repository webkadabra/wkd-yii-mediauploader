<?php
/**
 * MediaUploaderOwnerBehavior.php
 *
 * @author: sergii gamaiunov <hello@webkadabra.com>
 * @since: 10/14/13
 */

class MediaUploaderOwnerBehavior extends CActiveRecordBehavior
{
	protected $_tempId=null;
	protected function generateUserModelTempId() {

	}
	public function getUserModelTempId() {

	}
	public function afterFind($event) {
		if($this->owner->isNewRecord) {
			$this->generateUserModelTempId();
		}
	}
}