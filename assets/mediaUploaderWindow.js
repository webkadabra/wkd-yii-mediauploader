/**
 * CMS Uploader
 * @requires jquery-core jquery-ui-core jquery-ui-dialog
 * @provides mediaUploader
 * @author Sergii Gamaiunov <hello@webkadabra.com>
 */
var mediaUploaderWindow = function () {

    var options = {};
    // Default options:
    var defaults = {
        refreshDialog:true,
    };
    var browserInitiated = false;
    var $dialog, $dialogFrame, modalRemoved;

    function prepareUploaderDialog() {
        //console.log('prepareUploaderDialog()');
        var UploaderWindowUrl = options.browseUrl;
        if (options.modelName) {
            UploaderWindowUrl += '&modelName=' + options.modelName;
        }
        if (options.modelID) {
            UploaderWindowUrl += '&modelID=' + options.modelID;
        }
        if (options.modelModuleID) {
            UploaderWindowUrl += '&modelModuleID=' + options.modelModuleID;
        }
        if (options.tempID) {
            UploaderWindowUrl += '&tempID=' + options.tempID;
        }
        $dialog.load(UploaderWindowUrl, function () {
            if (typeof globalMediaUploader != 'undefined') {
                //console.log('prepareUploaderDialog(): Refresh uploader on dialog load: ' + globalMediaUploader.refresh());
            }// else
            //console.log('prepareUploaderDialog(): globalMediaUploader is undefined, can not refresh uploader on dialog load');
        });


    }

    /**
     * Kickstart app
     * @param array Options
     */
    function run(o)
    {
        var self = this;

        options = $.extend({}, defaults, o || {});
        //console.log(options);

        // Register TinyMCE
        if (typeof tinymce != 'undefined') {
            //mediaUploaderWindow.registerTinyMCEPlugin_3x();
            mediaUploaderWindow.registerTinyMCEPlugin_4x();
        }

        //var modalRemoved=false;
        $dialog = $('<div></div>');
        $dialog.dialog({
            title:"Files",
            dialogClass: "wkd-uploader-popup",
            modal:true,
            autoOpen:false,
            resizable:true,
            draggable:true,
            height:800,
            width:900,
            buttons:{
                //"Отмена": function() {
                //	$dialog.dialog("close");
                //},
            },
            // Remove modal overlay when dialog window is dragged, but switch it back on when dialog is closed
            //dragStart: function(event, ui) {
            /* if(modalRemoved==false) {
             modalRemoved = true;
             $dialog.dialog("option", "modal", false);
             $(".ui-widget-overlay").remove();
             } */
            //},
            /* dragStop: function(event, ui) {
             if ( typeof globalMediaUploader != 'undefined' ) {
             globalMediaUploader.refreshUploader();
             } else {
             alert('can\'t refresh uploader');
             }
             },
             resizeStop: function(event, ui) {
             if ( typeof globalMediaUploader != 'undefined' ) {
             globalMediaUploader.refreshUploader();
             }
             }, */
            close:function (event, ui) {
                //modalRemoved = false;
                $dialog.dialog("option", "modal", true);
            }
        });

        if(options.openOnDrag==true) {
            $('body').bind('dragenter', function(event) {
                    mediaUploaderWindow.open();
                    $("#uploader_filelist").addClass('drag-drop-zone-hl');
                    if (event.preventDefault) event.preventDefault();
                    /* event.dataTransfer.dropEffect = 'copy';
                     this.className = 'hovering';
                     */
                    return false;

                }
            ).bind('dragleave', function(event) {
                    if (event.preventDefault) {event.preventDefault()}
                    $("#uploader_filelist").removeClass('drag-drop-zone-hl');
                    return false;
                    /* event.dataTransfer.dropEffect = 'copy';
                     this.className = 'hovering';
                     */
                });
        }


        $dialog.delegate('a.ux-data-dismiss-dialog', 'click', function (e) {
            $dialog.dialog("close");
        });
        /**
         * data-options of element ('a.js-send-to-editor') :
         * data-code-target
         */
        $dialog.delegate('a.js-send-to-editor', 'click', function (e) {
            e = e || window.event;
            e.preventDefault();

            if (!e.currentTarget) {
                return;
            }

            var el = $(this);
            if(el.attr('data-code-target')) {
                var insertCodeEl = $(el.attr('data-code-target'));
            } else {
                // then it must be a grid
                var $parentRow = $(e.currentTarget).parent().parent();
                var insertCodeEl = $('.js-insert-code', $parentRow);
            }

            if (insertCodeEl) {
                var insertHtml = insertCodeEl.val();
                mediaUploaderWindow.sendToEditor(insertHtml);
            }
            /* console.log(test);
             return;
             var href = e.currentTarget.href;
             if(href) {
             var match = href.match(/\d+/);
             var id = parseInt(match);
             if(id) {

             var insertHtmlHolder = jQuery('#uploadInsertCode' + id);
             var insertHtml = insertHtmlHolder.val();
             console.log(insertHtmlHolder);
             //console.log(insertHtml);
             mediaUploaderWindow.sendToEditor(insertHtml);
             }
             }
             console.log(test);
             return;
             var href = e.currentTarget.href;
             if(href) {
             var match = href.match(/\d+/);
             var id = parseInt(match);
             if(id) {

             var insertHtmlHolder = jQuery('#uploadInsertCode' + id);
             var insertHtml = insertHtmlHolder.val();
             console.log(insertHtmlHolder);
             //console.log(insertHtml);
             mediaUploaderWindow.sendToEditor(insertHtml);
             }
             } */
            //var win = window.dialogArguments || opener || parent || top;
            //win.mediaUploaderWindow.sendToEditor(insertHtml);
            return;
        });

        /* $dialog = $('<div></div>').load(UploaderWindowUrl, function() {

         $dialog.delegate('a.js-send-to-editor', 'click', function(e) {
         e = e || window.event;
         e.preventDefault();

         if(!e.currentTarget) {
         return;
         }

         var href = e.currentTarget.href;
         if(href) {
         var match = href.match(/\d+/);
         var id = parseInt(match);
         if(id) {
         var insertHtmlHolder = $('#uploadInsertCode' + id);
         var insertHtml = insertHtmlHolder.val();
         self.sendToEditor(insertHtml);
         }
         }
         //var win = window.dialogArguments || opener || parent || top;
         //win.mediaUploaderWindow.sendToEditor(insertHtml);
         return;
         });
         });

         $dialog.dialog({
         title: "Файлы",
         dialogClass: "uploader-popup",
         modal: true,
         autoOpen: false,
         resizable: true,
         draggable: true,
         height: 800,
         width: 900,
         buttons: {
         //"Отмена": function() {
         //	$dialog.dialog("close");
         //},
         },
         // Remove modal overlay when dialog window is dragged, but switch it back on when dialog is closed
         dragStart: function(event, ui) {
         if(modalRemoved==false) {
         modalRemoved = true;
         $dialog.dialog("option", "modal", false);
         $(".ui-widget-overlay").remove();
         }
         },
         close: function(event, ui) {
         modalRemoved = false;
         $dialog.dialog("option", "modal", true);
         },

         }); */
    }

    function setModelID(id) {
        options.modelID = id
    }

    function setModelName(id) {
        options.modelName = id
    }

    function setModelModuleID(id) {
        options.modelModuleID = id
    }

    function setTempID(id) {
        options.tempID = id
    }

    /**
     *
     */
    function initBrowser() {
        if (browserInitiated)
            return;

        // Mark browser as initiated
        browserInitiated = true;
    }

    return {

        setModelID:setModelID,
        setModelName:setModelName,
        setModelModuleID:setModelModuleID,
        setTempID:setTempID,

        initialize:run,

        /**
         * Открываем окно загрузчика
         */
        open:function (e) {
            //console.log('Uploader window trigger clicked');
            prepareUploaderDialog();
            $dialog.dialog("open");

            if (typeof globalMediaUploader != 'undefined') {
                //console.log('(on dialog open) Refresh uploader: ' + globalMediaUploader.refreshUploader());
                globalMediaUploader.refreshUploader();
            }
            /* else
             console.log('(on dialog open)  globalMediaUploader is undefined, can not refresh uploader '); */

            return true;

            /* $dialog =
             jQuery.FrameDialog.create({
             url: $('#uploaderTrigger').attr('href'),
             loadingClass: 'loading-image',
             title: 'Файлы',
             width: 800,
             height: 600,
             autoOpen: false,
             buttons: {},
             modal: true,
             dialogClass: "uploader-popup",
             resizable: true,
             draggable: true,

             dragStart: function(event, ui) {

             $dialog.dialog("option", "modal", false);
             $(".ui-widget-overlay").remove();

             },
             close: function(event, ui) {
             return true;
             }
             });
             $dialog.dialog('open');
             return false; */
            //setTimeout('window.globalMediaUploader.refresh();', 1000);
            //setTimeout('globalMediaUploader.refresh();', 1000);
        },
        sendToEditor:function (html) {
            var ed;
            if (!html) {
                if (console) {
                    console.log('NO CONTENT!');
                }
                return;
            }
            if (typeof tinyMCE != 'undefined' && ( ed = tinyMCE.activeEditor ) && !ed.isHidden()) {
                ed.focus();
                if (tinymce.isIE)
                    ed.selection.moveToBookmark(tinymce.EditorManager.activeEditor.windowManager.bookmark);

                ed.execCommand('mceInsertContent', false, html);

            } else if (typeof edInsertContent == 'function') {
                edInsertContent(edCanvas, html);
            } else {
                alert('Ошибка: не получилось вставить текст');
            }
        },

        /**
         * Легко и непринуждённо создаём плагин для TinyMCE: в редактор добавляется кнопка mediaUploader, по клику
         * на которую запускается mediaUploaderWindow.open (кнопку нужно добавить в редактор отдельно, см. опцию
         * TinyMCE: theme_advanced_buttons1/theme_advanced_buttons2/theme_advanced_buttons3)
         */
        registerTinyMCEPlugin_4x:function ()
        {
            /* tinymce.PluginManager.add('example', function(editor, url) {
             // Add a button that opens a window
             editor.addButton('example', {
             text: 'My button',
             icon: false,
             onclick: function() {

             // Open window
             editor.windowManager.open({
             title: 'Example plugin',
             body: [
             {type: 'textbox', name: 'title', label: 'Title'}
             ],
             onsubmit: function(e) {
             // Insert content when the window form is submitted
             editor.insertContent('Title: ' + e.data.title);
             }
             });
             }
             });

             // Adds a menu item to the tools menu
             editor.addMenuItem('example', {
             text: 'Example plugin',
             context: 'tools',
             onclick: function() {
             // Open window with a specific url
             editor.windowManager.open({
             title: 'TinyMCE site',
             url: 'http://www.tinymce.com',
             width: 800,
             height: 600,
             buttons: [{
             text: 'Close',
             onclick: 'close'
             }]
             });
             }
             });
             }); */

            tinymce.PluginManager.add('uploadManager', function(editor, url) {
                // Add a button that opens a window
                editor.addButton('uploader', {
                    //text: 'My button',
                    icon: 'browse',
                    onclick: mediaUploaderWindow.open
                });
            });

        },

        /**
         * Легко и непринуждённо создаём плагин для TinyMCE: в редактор добавляется кнопка mediaUploader, по клику
         * на которую запускается mediaUploaderWindow.open (кнопку нужно добавить в редактор отдельно, см. опцию
         * TinyMCE: theme_advanced_buttons1/theme_advanced_buttons2/theme_advanced_buttons3)
         */
        registerTinyMCEPlugin_3x:function () {
            tinymce.create('tinymce.plugins.mediaUploaderPlugin', {
                createControl:function (n, cm) {
                    switch (n) {
                        case 'mediaUploader':
                            var c = cm.createMenuButton('mediaUploader', {
                                title:options.toolbar_button_title,
                                image:options.toolbar_button_img,
                                onclick:mediaUploaderWindow.open
                            });

                            // Return the new menu button instance
                            return c;
                    }
                    return null;
                }
            });

            // Register plugin with a short name
            tinymce.PluginManager.add('mediaUploader', tinymce.plugins.mediaUploaderPlugin);
        }
    }
}();