<?php
/**
 * MediaUploaderWidget is a Widget/JS plugin that also supports TinyMCE;
 * 
 * If you are using TinyMCE and you want to have "Uploader" button @ TinyMCE tooldbar
 * just add 'mediaUploader' button to one of your custom toolbars
 * 
 * @author Sergii Gamaiunov <hello@webkadabra.com>
 */
class MediaUploaderWidget extends CWidget
{
	public $options=array();
	public $scriptUrl=null;
	public $boundModel=null;
	
	/**
	 * @var bool Force assets not to be overwritten even if in YII_DEBUG mode � it is useful for heavily loaded int servers
	 */
	public $forceAssetsCache=false;
	public function run()
	{
		$cs = Yii::app()->getClientScript();
		$assets = Yii::app()->assetManager->publish(__DIR__ . DIRECTORY_SEPARATOR . 'assets', false, -1, ($this->forceAssetsCache == false ? YII_DEBUG : false));
		
		if($this->scriptUrl===null)
			$this->scriptUrl=$assets.'/mediaUploaderWindow.js';

		Yii::app()->getClientScript()->registerCoreScript('jquery.ui');
		if($this->scriptUrl!==false)
			Yii::app()->getClientScript()->registerScriptFile($this->scriptUrl);
			Yii::app()->getClientScript()->registerCssFile($assets.'/styles.css');

		$id=$this->getId();

		$defaults=array(
			'toolbar_button_title'=>'Insert/upload file',//t('wkd|uploader.toolbar_button_title'), //���������/�������� ����
			'toolbar_button_img'=>$assets.'/img/uploader_toolbar_btn_24x24.png',
			
			'uploadUrl'=>'',
			'browseUrl'=>'',
		);
		
		$options = array_merge($defaults, $this->options);
		
		/* if($this->boundModel) {
			$options['modelName'] = get_class($this->boundModel);
			$options['modelID'] = $this->boundModel->id;
		} */
		$cs->registerScript(__CLASS__.'#'.$id, "mediaUploaderWindow.initialize(".CJavaScript::encode($options).");", CClientScript::POS_READY);
	}
}