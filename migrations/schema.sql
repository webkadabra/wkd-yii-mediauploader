
SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `media_uploader_file`;
CREATE TABLE `media_uploader_file` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `folder` varchar(100) DEFAULT NULL,
  `file_name` varchar(100) NOT NULL,
  `original_filename` varchar(100) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Last edited time',
  `owner_user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT 'Upload title (optional)',
  `caption` text COMMENT 'Upload description',
  `ext` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `media_uploader_file_model`;
CREATE TABLE `media_uploader_file_model` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`file_id` int(11) unsigned NOT NULL,
    `model_pk` int(10) unsigned NOT NULL,
    `model_class_name` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;