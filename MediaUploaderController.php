<?php
/**
 * Basic MediaUploader Controller (based on Webkadabra CMS uploader)
 * To use this controller you can either copy it (or use provided class-based actions) OR set application's config 'controllerMap' as follows:
 * ~~~
 * 'controllerMap'=>array(
 *		'mediaUploader'=>array(
 *			'class'=>'ext.wkd.components.media-uploader.MediaUploaderController',
 *		),
 *	),
 * ~~~
 * 
 * @author b3atb0x <hello@webkadabra.com>
 */
class MediaUploaderController extends CExtController
{
	public function init()
	{
		Yii::import('wkd.components.media-uploader.models.*');
		parent::init();
	}

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			'index'=>array(
				'class'=>'wkd.components.media-uploader.actions.BrowseAction',
			),
			'uploadAttachment'=>array(
				'class'=>'wkd.components.media-uploader.actions.UploadFileAction',
				'folder'=>'frontend.www.media.uploads.blog.' . date('Ym').'.'.date('d'),
				'frontendFolder'=>'/media/uploads/blog/' . date('Ym').'/'.date('d'),
				'modelClassName'=>'UploadImageForm',
				'modelImportAlias'=>'wkd.components.media-uploader.models.form.UploadImageForm',
				'modelAttribute'=>'uploadedFile',
				'uploadedAttributeName'=>'file',
				//'onSuccessUpload'=>'onFileUploaded',
			),
		);
	}
}