<?php
/**
 * index.php
 *
 * Author: Sergii Gamaiunov <hello@webkadabra.com>
 * Date: 21.05.13
 * Time: 17:03
 */
?>
<div class="pull-right">
    <a type="button" class="close btn btn-small ux-data-dismiss-dialog">&times;</a>
</div>
<?php
$this->widget('bootstrap.widgets.TbTabView', array(
	'type' => 'tabs',
	'menuHtmlOptions' => array('style'=>'
		margin: -6px -13px 20px;
		background: #f0f0f0;
		padding-top: 5px;'),
	'placement' => 'top',
	'tabs' => array(
		array('label' => Yii::t('uploader', 'Upload'), 'view' => 'upload', 'data' => array(
			'model'=>$model,
			'boundModel' => (!empty($boundModel) ? $boundModel : null),
		)),
		array('label' => Yii::t('uploader', 'Browse'), 'view' => 'browse', 'data' => array(
			'model' => $model,
		)),


	)
));
?>
<style>
    .drag-drop-zone-hl {
        border: 3px dashed #3399CC;

        border-radius: 7px;
        -webkit-border-radius: 7px;
        -moz-border-radius: 7px;
    }
</style>