<?php
/**
 * _browse_server.php
 *
 * Author: Sergii Gamaiunov <hello@webkadabra.com>
 * Date: 21.05.13
 * Time: 17:54
 */
?>
<div class="row-fluid">
    <div class="span5">
        <div id="filtesTree" style="max-height: 500px;overflow-y: auto;"></div>
    </div>
    <div class="span7">

		<div class="well well-small" id="server-filebrowser-controles" style="display:none">
			<a href="#" id="filebrowse_submit" data-code-target="#uploadInsertCode_browse" class="btn js-send-to-editor">Выбрать это изображение</a>
		</div>

        <img id="filebrowse_preview" style="display:none"/>
    </div>
</div>
<?
$this->widget('wkd.widgets.file-tree.WkdFileTree', array(
	'target' => '#filtesTree',
	'options' => array(
		'root' => '/img/',
		'script' => '/site/browseFrontend',
	),
	'callback' => 'function(file) {
		file = "' . rtrim(app()->params['frontend.url'], '/') . '" + file;
		$("#filebrowse_preview").attr("src", file);
		$("#uploadInsertCode_browse").val("<img src=\""+file+"\" />");
		$("#filebrowse_preview").show();
		$("#server-filebrowser-controles").show();
	}'
));
echo CHtml::hiddenField("uploadInsertCode_browse", '', array("id"=>"uploadInsertCode_browse"));
?>