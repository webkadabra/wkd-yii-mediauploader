<?php
echo CHtml::form('', 'POST', array('enctype' => 'multipart/form-data'));
if (!empty($boundModel)) {
	$uploadUrl = $this->createUrl('uploadAttachment', array('modelName' => get_class($boundModel), 'modelID' => $boundModel->id));
} else
	$uploadUrl = $this->createUrl('uploadAttachment');

$this->widget('wkd.3rdparty.plupload.PluploadWidget', array(
	'config' => array(
		#'sessionParam'=>Yii::app()->getSession()->getSessionName(),
		#'scriptData' => array('PHPSESSID' => session_id()),
		#'scriptData'=>array(
		#	'entity_id'=>$model->id,
		#),
		'runtimes' => 'html5,flash,silverlight,browserplus',
		'url' => $uploadUrl,
		//'max_file_size' => str_replace("M", "mb", ini_get('upload_max_filesize')),
		'max_file_size' => isset(Yii::app()->params['maxFileSize']) ? Yii::app()->params['maxFileSize'] : str_replace("M", "mb", ini_get('upload_max_filesize')),
		'chunk_size' => '1mb',
		'unique_names' => false,
		'filters' => array(
			array('title' => Yii::t('app', 'Images files'), 'extensions' => 'jpg,jpeg,gif,png'),
		),
		'language' => (Yii::app()->language !== 'en' ? Yii::app()->language : null),
		//'max_file_number' => $maxImages,
		'autostart' => true,
		'jquery_ui' => false,
		'reset_after_upload' => true,
		/* 'resize'=>array(
			'width'=>1680, 
			'height'=>1050,
			'quality'=>90
		), */
	),
	'callbacks' => array(
		'FileUploaded' => 'onFileUploaded',
	),
	'id' => 'uploader'
));?>

<script>
function onFileUploaded(up,file,response) {
	/* if(response.response) {
		var res = $.parseJSON(response.response);
		$("#imagesContainer").append('<li class="span3"><a href="#" class="thumbnail"><img src="'+res.link+'"></a><br><label><input type="checkbox" id="Good_updatedimage_'+res.id+'" name="Good[updatedimage]['+res.id+']" value="1"> Удалить?</label>');
	} */
	var uploadedData = jQuery.parseJSON(response.response);
	jQuery.fn.yiiGridView.update("user-grid");
	UX.notify('Файл загружен');
	/* jQuery.pnotify({
		pnotify_title: 'Файл загружен',
	}); */
}
</script>

<ul class="thumbnails clearfix" id="imagesContainer">
</ul>

<?php /* $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'itemsTagName'=>'ul',
));  */?>

<div id="globalUploaderGridContainer">
	<?php $this->renderPartial('_grid', array('model'=>$model)); ?>
</div>

<?php echo CHtml::endForm(); ?>