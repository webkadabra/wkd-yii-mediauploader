<?php
/**
 * browse.php
 *
 * Author: Sergii Gamaiunov <hello@webkadabra.com>
 * Date: 21.05.13
 * Time: 17:05
 */
?>
<?
$switchButtons=$this->widget('bootstrap.widgets.TbButtonGroup', array(
	'htmlOptions' => array(
		#'class'=> 'pull-right',
		#'style'=>'margin-top:-6px',
	),
	'type' => '',
	'toggle' => 'radio',
	'buttons' => array(
		array('label' => 'Everything Uploaded', 'active'=>true,
			'htmlOptions'=>array(
				'class'=>'btn-control-switch ux-set-wo-to-normal',
				'data-switch'=>'h:.ux-browser-section-server|s:.ux-browser-section-uploaded',
				'title'=>'Browse all uploaded files',
				'rel'=>'tooltip',
			)),
		array('label' => 'Files on server', 'active'=>false,
			'htmlOptions'=>array(
				'class'=>'btn-control-switch ux-set-wo-to-loose',
				'data-switch'=>'s:.ux-browser-section-server|h:.ux-browser-section-uploaded',
				'title'=>'Browse files on server (theme & design images etc.)',
				'rel'=>'tooltip',
			)),
	),
), 1);
echo $switchButtons;
?>
<div class="ux-browser-section-uploaded">
	<? $this->renderPartial('_browse_uploads',array(
		'model'=> new MediaUploaderFile()
	));?>
</div>

<div class="ux-browser-section-server" style="display:none">
	<? $this->renderPartial('_browse_server',array(

	));?>
</div>