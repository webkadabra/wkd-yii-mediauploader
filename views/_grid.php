<style>
    .hidden_el {
        display: none
    }
</style>

<?php 
Yii::app()->clientScript->registerScript('hide_invalid_links','if ( typeof tinyMCE == "undefined" ) { $(".js-send-to-editor").hide()} ', CClientScript::POS_READY);
$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'summaryText'=>'Отображаются {start}-{end} из {count} файлов.',
	'itemsCssClass'=>'table',

	'cssFile'=>false,

	//<a class="btn" id="globalMediaUploaderTriggerButton" onclick="bareTriggerClick(event)" href="#" ><span>+ Добавить файл</span></a>'
	'template'=>'<br clear="both" />{pager}
	{items}
	<div style="float:right">
		{summary}
	</div>
	<br clear="both" />
	{pager}',

	//'afterAjaxUpdate'=>'function(){adminAdvancedGridView.refresh()}',

	/* 'itemsCssClass'=>'global fit underline',
	'pagerCssClass'=>'gridPagination',
	'pager'=>array(
		'cssFile'=>false,
		'htmlOptions'=>array('class'=>'gridPager'),
	), */

	'columns'=>array(
		/* array(
			'class'=>'CCheckBoxColumn',
			'selectableRows'=>2,
			'checkBoxHtmlOptions'=>array('name'=>'batchIDs[]'),
		), */
		array(
			'type'=>'raw',
			'value'=>'($data->isImage() ? CHtml::image($data->getLink(), null, array("style"=>"max-height:30px")) : null)
				.	 CHtml::hiddenField("uploadInsertCode".$data->id, $data->getInsertCode(), array("class"=>"js-insert-code"))',
		),
		WkdHelper::editableColumn('name'),
//		array(
//			'name'=>'name',
//			'type'=>'raw',
//			'value'=> '"<span id=\'upload_name_".$data->id."\'>".$data->getName()."</span>" '
//				. ' . CHtml::textField("upload_rename_".$data->id, $data->getName(), array("class"=>"hidden_el")) '
//				. ' . CHtml::textArea("upload_caption_".$data->id, $data->caption, array("class"=>"hidden_el")) '
//				. ' . CHtml::hiddenField("uploadInsertCode".$data->id, $data->getInsertCode(), array("class"=>"js-insert-code"))',
//		),

		array(
			'header'=>Yii::t('uploader', 'Link'),
			'type'=>'raw',
			'value'=>'CHtml::textField("no", $data->getLink(), array("onclick"=>"this.select()","class"=>"span3"))',
		),
		array(
			'class'=>'CButtonColumn',
			'buttons'=>array(
				'sendToEditor'=>array(
					//'imageUrl'=>'/static/img/1312431698_insert-image.png',
					'label'=>'<i class="icon icon-picture"></i> ' . Yii::t('uploader', 'Paste into editor'),
					'url'=>'$data->id',
					'options'=>array(
						'class'=>'js-send-to-editor btn',
						'title'=>'<i class="icon icon-picture"></i>' . Yii::t('uploader', 'Paste into editor'),
					),
				),
			),
			'template'=>'{sendToEditor} ',
			'header'=>Yii::t('uploader', 'Tools'),
			'htmlOptions'=>array('align'=>'center', 'width'=>'1%', 'class'=>'nowrap'),
		),
	),
)); ?>