Webkadabra Media Uploader
==========================
##### Author: Sergii Gamaiunov <hello@webkadabra.com>
##### [http://webkadabra.com](http://webkadabra.com)

Introduction
======================


Installation
=======================

1. Preparation
-----------------------

1. Copy contents of this folder to /path/to/extensions/wkd/components/media-uploader folder
	
2. Install MySql schema

See database.sql

3. Make sure your Yii application has 'wkd' path alias pointed to `/path/to/extensions/wkd/`

2. Usage Example
-----------------------

1. Add MediaUploader to Controller map in app config:
	
	
```
	'controllerMap'=>array(
		'mediaUploader'=>array(
			'class'=>'wkd.components.media-uploader.MediaUploaderController',
		),
	),
```
	
2. Add path alias for tmp uploads folder in config:


```
	'params'=>array(
		...
		'envTempDir' => 'application.runtime', // uploader temp folder
	),
```
	
3. In your views, add uploader widget:
	
	
```
	// Uploader, Must be called before RTEditor:
	$this->widget('wkd.components.media-uploader.MediaUploaderWidget', array(
		'options' => array(
			'uploadUrl' => app()->createUrl('mediaUploader/upload', array('modelName' => get_class($model), 'modelID' => $model->id)),
			'browseUrl' => app()->createUrl('mediaUploader/index', array('modelName' => get_class($model), 'modelID' => $model->id)),
		),
		'boundModel' => $model,
		#'forceAssetsCache'=>true,
	));
```
	
4. To open up an uploader window, call Javascript helper function `mediaUploaderWindow.open()`
