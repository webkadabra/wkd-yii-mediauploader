<?php
/**
 * Change uploaded file caption
 */
class ChangeUploadedFileCaptionAction extends CAction
{
	public $uploadModelClassName = NULL;

	/**
	 * Приём отправенных на загрузку файлов
	 */
	public function run($id=null)
	{
		// Connect this upload to ActiveRecord
		if(!$this->uploadModelClassName) {
			throw new CHttpException(500,'Upload model is not specified');
		}
		
		if(Yii::app()->request->isPostRequest AND $_POST['setName'])
		{
			$uploadModelClassName = $this->uploadModelClassName;
			
			$attachmentID = intval($_POST['attachmentID']);
			
			if(!$attachmentID) {
				throw new CHttpException(404,'Invalid request for attachment ' . $attachmentID);
			}
			if(!$attachment = $uploadModelClassName::model()->findByPk($attachmentID)) {
				throw new CHttpException(404,'Attachment '.$attachmentID.' not found');
			}
			$attachment->caption = $_POST['setName'];
			
			if($attachment->save()) {
			
			} else {
				throw new CHttpException(500,'Can not save attachment');
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
}