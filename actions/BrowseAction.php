<?php
/**
 * Action to browse uploaded files
 * @author Sergii Gamaiunov <hello@webkadabra.com>
 */
class BrowseAction extends CAction
{
	/**
	 * 
	 */
	public function run($modelName=null,$modelID=null,$modelModuleID=null,$tempID=false)
	{
		if($modelModuleID !== null) {
			if($module = app()->getModule($modelModuleID)) {
				$module->init();
			}
		}
		if($modelName && $modelID && class_exists($modelName)) {
			$boundModel = $modelName::model()->findByPk($modelID);
		}
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
			unset($_GET['pageSize']);
		}
		
		$model=new MediaUploaderFile('search');
		if(isset($_GET['MediaUploaderFile'])) {
			$model->attributes=$_GET['MediaUploaderFile'];
		}
		else
			$model->unsetAttributes();
		
		if(!empty($boundModel)) {
			$model->filterByModel[$modelName] = $modelID;
		} else if($tempID) {
			$model->filterByTempID = $tempID;
		}
		
		$dataProvider=$model->search();
		
		 // Check if this is an AJAX request and turn off some scripts if it is
		if( Yii::app()->request->isAjaxRequest )
		{
			// Create default array for scripts which should be disabled
			$defaultDisableScripts = array(
				'jquery.js',
				'jquery.min.js',
				'jquery-ui.min.js'
			);

			// Disable scripts
			foreach( $defaultDisableScripts as $script )
				Yii::app()->clientScript->scriptMap[$script] = false;

		}
		if(app()->request->isAjaxRequest OR isset($_GET['dialog'])) {
			$this->controller->renderPartial('index', array(
				'model'=>$model,
				'dataProvider'=>$dataProvider,
				'boundModel'=>!empty($boundModel) ? $boundModel : null,
				'modelModuleID'=>$modelModuleID ? $modelModuleID : null,
				'tempID'=>$tempID ? $tempID : false,
			),false,true);
		} else {
			$this->controller->render('index', array(
				'model'=>$model,
				'dataProvider'=>$dataProvider,
				'boundModel'=>!empty($boundModel) ? $boundModel : null,
				'modelModuleID'=>!empty($modelModuleID) ? $modelModuleID : null,
				'tempID'=>!empty($tempID) ? $tempID : false,
			));
		}
	}
}