<?php
/**
 * Action to catch uploading files
 * @author Sergii Gamaiunov <hello@webkadabra.com>
 */
class UploadFileAction extends CAction
{
	/* public $boundModelClassName = NULL;
	public $uploadModelClassName = NULL;
	public $boundModelAttributeName = NULL;
	 */
	/**
	 * @var string the name of a function of a Controller (Yii::app()->getController()) that will be called after upload is saved
	 */
	public $onSuccessUpload = null;

	public $folder = null;
	public $frontendFolder = null; // TODO: Rename to 'publicFolder' - more clearly
	/**
	 * @var null Model/Form class name of uploaded file
	 */
	public $modelClassName = null;
	/**
	 * @var null
	 */
	public $modelImportAlias = null;
	/**
	 * @var null attribute name of a $modelClassName that has uploaded file (i.e. $_FILES[uploadedFile])
	 */
	public $modelAttribute = 'uploadedFile';
	public $uploadedAttributeName = null;
	/**
	 * Whether or not uploaded file should be saved as MediaUploaderFile model
	 */
	public $saveUploadRecord = true;

	/**
	 * Приём отправенных на загрузку файлов
	 */
	public function run($modelName = null, $modelID = null, $modelModuleID = null, $tempID = false, $id = null)
	{
		//Yii::trace(print_r($_REQUEST, true), 'uploader_debug');
		//Yii::trace(print_r($_FILES, true), 'uploader_debug');
		if ($modelModuleID !== null) {
			if ($module = app()->getModule($modelModuleID)) {
				$module->init();
			}
		}
		// Connect this upload to $modelName Record
		// Make sure that $modelName is available (maybe you'd have to import it in config, if it is not in default application.model folder)
		if ($modelName && $modelID && class_exists($modelName)) {
			$boundModel = $modelName::model()->findByPk($modelID);
		}

		// Check that we have the minimum to work here:
		if ($this->folder === null) {
			throw new CException('Please, specify folder for uploaded file to be stored at');
		}
		if ($this->modelClassName === null) {
			throw new CException('Please, specify $modelClassName option');
		}

		if ($this->modelImportAlias) {
			Yii::import($this->modelImportAlias);
		}

		// Connect this upload to ActiveRecord
		/* if($this->boundModelClassName AND isset($_REQUEST['item'])) {
			$modelClassName = $this->boundModelClassName;
			$model = $modelClassName::model()->findByPk($_REQUEST['item']);
		} */

		// Ignore user aborts and allow the script to run forever
		ignore_user_abort(true);
		set_time_limit(999);

		// HTTP headers for no cache etc
		header('Content-type: text/plain; charset=UTF-8');
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		// Setup uploader
		$targetDir = (Yii::app()->params['envTempDir'] ? Yii::getPathOfAlias(Yii::app()->params['envTempDir']) : sys_get_temp_dir()) . DIRECTORY_SEPARATOR . "plupload";
		#$targetDir = realpath(Yii::app()->uploader->getTempDirPath());

		$cleanupTargetDir = false; // Remove old files
		$maxFileAge = 60 * 60; // Temp file age in seconds

		// Create target dir
		if (!file_exists($targetDir))
			@mkdir($targetDir, 0777, true);

		if (!is_writable($targetDir))
			die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Can not write to target dir."}, "id" : "id"}');

		// Get parameters
		$chunk = isset($_REQUEST["chunk"]) ? $_REQUEST["chunk"] : 0;
		$chunks = isset($_REQUEST["chunks"]) ? $_REQUEST["chunks"] : 0;
		$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

		$originalname = $fileName;

		// Clean the fileName for security reasons
		$fileName = preg_replace('/[^\w\._\s]+/', '', $fileName);

		/* if(!empty($model)) {
			$fileName = 'itm'.$model->id.'-'.$fileName;
		} */
		// Make sure the fileName is unique but only if chunking is disabled
		if ($chunks < 2 && file_exists($targetDir . '/' . $fileName)) {
			$ext = strrpos($fileName, '.');
			$fileName_a = substr($fileName, 0, $ext);
			$fileName_b = substr($fileName, $ext);

			$count = 1;
			while (file_exists($targetDir . '/' . $fileName_a . '_' . $count . $fileName_b))
				$count++;

			$fileName = $fileName_a . '_' . $count . $fileName_b;
		}

		// Remove old temp files
		if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
			while (($file = readdir($dir)) !== false) {
				$filePath = $targetDir . '/' . $file;

				// Remove temp files if they are older than the max age
				if (preg_match('/\\.tmp$/', $file) && (filemtime($filePath) < time() - $maxFileAge))
					@unlink($filePath);
			}
			closedir($dir);
		} else {
			die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
		}

		// Look for the content type header
		if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
			$contentType = $_SERVER["HTTP_CONTENT_TYPE"];

		if (isset($_SERVER["CONTENT_TYPE"]))
			$contentType = $_SERVER["CONTENT_TYPE"];

		// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
		if (strpos($contentType, "multipart") !== false) {


			if ($this->modelAttribute && $this->modelClassName) {
				$name = CHtml::activeName(new $this->modelClassName, $this->modelAttribute);
			} else {
				$name = $this->uploadedAttributeName;
			}

			$server_files_name = ($this->uploadedAttributeName ? $this->uploadedAttributeName : $this->modelClassName);

			if (!isset($_FILES[$server_files_name]['tmp_name'])) {
				throw new CHttpException(401, 'No file received[1]');
			}

			$tmp_file = (is_array($_FILES[$server_files_name]['tmp_name'])
				? $_FILES[$server_files_name]['tmp_name'][$this->modelAttribute]
				: $_FILES[$server_files_name]['tmp_name']);

			if (!is_uploaded_file($tmp_file)) {
				throw new CHttpException(401, 'No file received[2]');
			}

			// Open temp file
			$out = fopen($targetDir . '/' . $fileName, $chunk == 0 ? "wb" : "ab");
			if ($out) {
				// Read binary input stream and append it to temp file
				$in = fopen($tmp_file, "rb");

				if ($in) {
					while ($buff = fread($in, 4096))
						fwrite($out, $buff);
					fclose($in);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

				fclose($out);
				unlink($tmp_file);
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');

		} else {
			// Open temp file
			$out = fopen($targetDir . '/' . $fileName, $chunk == 0 ? "wb" : "ab");
			if ($out) {
				// Read binary input stream and append it to temp file
				$in = fopen("php://input", "rb");

				if ($in) {
					while ($buff = fread($in, 4096))
						fwrite($out, $buff);
					fclose($in);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

				fclose($out);
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
		}

		// After last chunk is received, process the file
		$response = array('uploaded' => false);

		if (intval($chunk) + 1 >= intval($chunks)) {
			#$originalname = $fileName;
			if (isset($_SERVER['HTTP_CONTENT_DISPOSITION'])) {
				$arr = array();
				preg_match('@^attachment; filename="([^"]+)"@', $_SERVER['HTTP_CONTENT_DISPOSITION'], $arr);
				if (isset($arr[1]))
					$originalname = $arr[1];
			}

			// **********************************************************************************************
			// Do whatever you need with the uploaded file, which has $originalname as the original file name
			// and is located at $targetDir . DIRECTORY_SEPARATOR . $fileName
			// **********************************************************************************************

			// VALIDATE:	
			$model = new $this->modelClassName;
			$attribute = $this->modelAttribute;

			if ($this->modelAttribute && $this->modelClassName) {
				$uploadedFile = CUploadedFile::getInstance(new $this->modelClassName, $this->modelAttribute);
				#dump($uploadedFile);exit;
			} else {
				$uploadedFile = CUploadedFile::getInstanceByName('file');
			}
			$model->attributes = array($attribute => '');
			$model->setAttributes(array(
				$attribute => $uploadedFile,
			));

			if ($model->validate() === false) {
				// Delete file?
				//@unlink($targetDir.'/'.$fileName);
				$errors = CHtml::errorSummary($model, false, false, array('firstError'));

				echo CJavaScript::jsonEncode(array(
					'jsonrpc' => '2.0',
					'error' => array(
						'code' => 102,
						'message' => Yii::t(__CLASS__, "Invalid file. " . $errors, array()),
					),
				));
				exit;
			}

			// File is OK - move to desired folder:
			$uploadFolder = $this->frontendFolder;
			$uploadPath = Yii::getPathOfalias($this->folder);

			if (!file_exists($uploadPath)) {
				@mkdir($uploadPath, 0775, true);
				if (!file_exists($uploadPath)) {
					die('cant create folder');
				}
			}

			// Move file from temporary filder to calculated media folder
			if (!rename($targetDir . '/' . $fileName, $uploadPath . '/' . $fileName)) {
				echo CJavaScript::jsonEncode(array(
					'jsonrpc' => '2.0',
					'error' => array(
						'code' => 102,
						'message' => "Failed to move: " . $targetDir . '/' . $fileName . " to " . $uploadPath . '/' . $fileName,
						'filename' => $fileName,
					),
				));
				exit;
			}

			// Ok, at this point wehave successfully uploaded file,validated and saved it.
			$uploadInfo = array(
				'attachedToModelId' => $id,
				'folder' => $uploadFolder, // Web-accessible folder
				'fileName' => $fileName, // Filename in that folder
				'href' => $uploadFolder . '/' . $fileName, // Full Web-accessible link
				'fullPath' => $uploadPath . DIRECTORY_SEPARATOR . $fileName, // Full path to file
				'dirPath' => $uploadPath . DIRECTORY_SEPARATOR, // Full path to file
			);

			if ($this->saveUploadRecord === true) {

				Yii::import('wkd.components.media-uploader.models.MediaUploaderFile');
				#require_once( dir(__FILE__) . DIRECTORY_SEPARATOR . '..'.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.'MediaUploaderFile.php');

				$saveModel = new MediaUploaderFile;
				$saveModel->folder = '/' . trim($this->frontendFolder, '/') . '/';
				$saveModel->file_name = $fileName;
				$saveModel->original_filename = (isset($originalname)) ? $originalname : $fileName;
				$saveModel->modified_on = new CDbExpression('NOW()');
				$saveModel->owner_user_id = Yii::app()->user->id;
				$saveModel->name = $fileName;
				$saveModel->ext = strtolower(CFileHelper::getExtension($saveModel->getLink()));
				$saveModel->save(false);

				if (!empty($boundModel)) {
					$saveModel->boundByModel($boundModel);
				}
			}
			if ($this->onSuccessUpload) {
				return Yii::app()->controller->{$this->onSuccessUpload}($uploadInfo);
			} else {
				unset($uploadInfo['folder'], $uploadInfo['path']); // No need to reveal this for the public

				$response['uploaded'] = true;
				$response['file'] = $uploadInfo;
			}
		}

//		$uploadInfo = array(
//			'attachedToModelId' => $id,
//			'folder' => $uploadFolder, // Web-accessible folder
//			'fileName' => $fileName, // Filename in that folder
//			'href' => $uploadFolder . '/' . $fileName, // Full Web-accessible link
//			'fullPath' => $uploadPath . DIRECTORY_SEPARATOR . $fileName, // Full path to file
//			'dirPath' => $uploadPath . DIRECTORY_SEPARATOR, // Full path to file
//		);
//		echo json_encode(array(array(
//			"name" => $model->{$this->displayNameAttribute},
//			"type" => $model->{$this->mimeTypeAttribute},
//			"size" => $model->{$this->sizeAttribute},
//			"url" => $this->getFileUrl($model->{$this->fileNameAttribute}),
//			"thumbnail_url" => $model->getThumbnailUrl($this->getPublicPath()),
//			"delete_url" => $this->getController()->createUrl($this->getId(), array(
//				"_method" => "delete",
//				"file" => $model->{$this->fileNameAttribute},
//			)),
//			"delete_type" => "POST"
//		)));

		echo CJavaScript::jsonEncode($response);
		exit;
	}
}