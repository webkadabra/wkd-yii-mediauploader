<?php
/**
 * Delete uploaded file
 */
class DeleteUploadedFileAction extends CAction
{
	public $uploadModelClassName = NULL;

	/**
	 * Приём отправенных на загрузку файлов
	 */
	public function run($id=null)
	{
		// Connect this upload to ActiveRecord
		if(!$this->uploadModelClassName) {
			throw new CHttpException(500,'Upload model is not specified');
		}
		
		if(Yii::app()->request->isPostRequest)
		{
			$attachmentID = intval($_POST['attachmentID']);
			
			$uploadModelClassName = $this->uploadModelClassName;
			
			if(!$attachmentID) {
				throw new CHttpException(404,'Invalid request for attachment ' . $attachmentID);
			}
			if(!$attachment = $uploadModelClassName::model()->findByPk($attachmentID)) {
				throw new CHttpException(404,'Attachment '.$attachmentID.' not found');
			}
			
			if($attachment->delete()) {
			
			} else {
				throw new CHttpException(500,'Can not delete attachment');
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
}